package com.company;

/**
 *
 */
public interface StatService {
    class StatResults {
        int averageAmountPerDay=0;
        int total=0;
        int total1=0;

        public int averagePerDay(int nextSums, int transNumber){

            //accumulating the total
            total=total+nextSums;
            //counts the average
            averageAmountPerDay=total/transNumber;

            return (averageAmountPerDay);
        }

        public int total(int nextSums, int transNumber){
            total1=total1+nextSums;

            return (total1);

        }

    }
    /**
     * @param unit use Calendar constants, i.e. Calendar.MONTH, Calendar.YEAR
     */
   // public StatResult calcStatistics(List<Transaction> transactions, int time, int unit);
}