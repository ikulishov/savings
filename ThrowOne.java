package com.company;

import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.Date;

/**
 * ThrowOne used only for throws
 */
public class ThrowOne {
    private SimpleDateFormat dateFormat = new SimpleDateFormat("yyyy-mm-dd");
    public Date transactionTime;
    public int amount; // in cents

    public ThrowOne(String transactionTimeStr, int amount) throws ParseException {
        this.transactionTime = dateFormat.parse(transactionTimeStr);
        this.amount = amount;
        System.out.println("the date is in string format");


            }

    public ThrowOne(Date transactionTime, int amount) {
        this.transactionTime = transactionTime;
        this.amount = amount;
        System.out.println("the initial date is in 'Date' format");



    }


}